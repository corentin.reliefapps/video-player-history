<?php

namespace App\Controller;

use App\Entity\History;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Doctrine\Common\Persistence\ObjectManager;

class HistoryController extends Controller
{
    public function getHistoryAction()
    {
        $historys = $this->getDoctrine()->getRepository('App:History')->findAll();
        $data = $this->get('jms_serializer')->serialize($historys, 'json');

        $response = new Response($data);
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }


    public function insertHistory(Request $request)
    {
        $data = $request->getContent();
        $history = $this->get('jms_serializer')->deserialize($data, 'App\Entity\History', 'json');

        $em = $this->getDoctrine()->getManager();
        $em->persist($history);
        $em->flush();

        return new Response('', Response::HTTP_CREATED);
    }
}
